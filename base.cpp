#include "base.h"
#include <cmath>

POINT CalcBarycenter( const std::deque<POINT> &points ) {
	POINT result{ 0, 0 };
	float S = 0.0f;

	if ( points.size() < 3 ) {
		result.x = points.front().x + ( ( points.front().x - points.back().x ) / 2 );
		result.y = points.front().y + ( ( points.front().y - points.back().y ) / 2 );
		return result;
	}

	for ( int i = 0; i < points.size() - 1; ++i ) {
		float mul = points[i].x * points[i + 1].y - points[i + 1].x * points[i].y;
		result.x += mul * ( points[i].x + points[i + 1].x );
		result.y += mul * ( points[i].y + points[i + 1].y );
		S += mul;
	}

	result.x /= ( S * 3 );
	result.y /= ( S * 3 );
	return result;
}

void USSleep( const DWORD &dwMilliseconds ) {
	auto exitTime =
		std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() )
			.count() +
		dwMilliseconds;
	while ( exitTime >
			std::chrono::duration_cast<std::chrono::milliseconds>( std::chrono::system_clock::now().time_since_epoch() )
				.count() )
		;
}
