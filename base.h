#ifndef BASE_H
#define BASE_H

#ifdef FULL_DX_HOOK
#include <ProxyDX9/ProxyDX9.h>
#endif
#ifdef USE_DRAW_HOOK
#include <DrawHook/DrawHook.h>
#endif
#include <d3d9.h>
#include <d3dx9.h>
#include <d3dx9core.h>
#include <llmo/callfunc.hpp>
#include <llmo/ccallhook.h>
#include <wingdi.h>

#include <SRCursor.h>
#include <SREvents.h>
#include <SRKeys.hpp>

struct stGlobalClasses {
	/// Указатель на объект IDirect3D9
	IDirect3D9 *&d3d = *reinterpret_cast<IDirect3D9 **>( 0xC97C20 );
	/// Указатель на D3DPRESENT_PARAMETERS. Там всякие нештяки типа разрешения экрана
	D3DPRESENT_PARAMETERS *params = reinterpret_cast<D3DPRESENT_PARAMETERS *>( 0xC9C040 );
#ifdef FULL_DX_HOOK
	/**
	 * \brief Указатель на proxyIDirect3DDevice9
	 * \detail Данный класс является хуком над оригинальным IDirect3DDevice9.
	 * Он используется для решистрации различных примитивов и получения оригинального указателя
	 * IDirect3DDevice9. Данный класс так же позволяет перенаправлять методы IDirect3DDevice9 в другие классы,
	 * используя систему сигналов и слотов. Если вы не хотите, что бы после вызова вашего хука, вызывался
	 * оригинальный метод (например, если вы вызываете его сами), то дергайте метод d3d9_hook
	 */
	hookIDirect3DDevice9 *DirectX = static_cast<hookIDirect3DDevice9 *>( nullptr );
#endif
#ifdef USE_DRAW_HOOK
	/// Указатель на хук рисования
	DrawHook *draw = static_cast<DrawHook *>( nullptr );
#endif
	/// Объект для управления вызовом курсора на экран. Не зависит от версии сампа
	SRCursor *cursor = nullptr;
	/**
	 * \brief Указатель на класс обработки различных событий
	 * \details Данный класс позволяет перехватывать события окна игры, выполнять свой код во время работы
	 * mainloop и scriptloop, обрабатывать нажатия клавиш и хукать события RakNet
	 */
	SREvents *events = static_cast<SREvents *>( nullptr );
};
struct stGlobalPVars {
	/// Номер состояния игры. 9 - загружена
	uint32_t &gameSatate = *reinterpret_cast<uint32_t *>( 0xC8D4C0 );
	/// Ссылка на состояние игрового меню (открыто/закрыто)
	bool &isMenuOpened = *reinterpret_cast<bool *>( 0xBA67A4 );
	/// Ссылка на номер активного пункта в меню игры
	byte &activeMenuID = *reinterpret_cast<byte *>( 0xBA68A5 );
	bool &gamePaused   = *reinterpret_cast<bool *>( 0xB7CB49 );
	/// Ссылка на hwnd окна игры
	HWND &hwnd = *reinterpret_cast<HWND *>( 0xC97C1C );
	/// Флаг, указывающий работает ли игра в окне
	int &windowed = *reinterpret_cast<int *>( 0xC920CC );
	/// Глобальные состояния движка
	class RsGlobalType *rs = (RsGlobalType *)0xC17040;
};

/// Название проекта, которое было указано при разворачивание проекта из шаблона
extern const std::string_view PROJECT_NAME;

/// Содержит указатели на базовые классы
extern stGlobalClasses g_class;
/// Содержит указатели на базовые переменные игры
extern stGlobalPVars g_vars;

/**
 * @brief Находит барицентр (центроид) полигонной фигуры
 * @param points вершины полигона
 * @return координаты центра полигона
 */
POINT CalcBarycenter( const std::deque<POINT> &points );

void USSleep( const DWORD &dwMilliseconds );

#endif // BASE_H
