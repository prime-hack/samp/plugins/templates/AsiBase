#ifndef MVAR_H
#define MVAR_H

#include <windows.h>

template<class C>
class MVar
{
public:
	MVar(unsigned int offset, HMODULE module = 0) : offset(offset), module(module) {
		hasFirstValueSaved = false;
		if (!module && offset){
			firstValue = read(); // yeahhh....
			defaultProtect = getProtection();
		}
	}

	inline void operator=(C *src)
	{
		offset = reinterpret_cast<unsigned int>(src);
		firstValue = read();
		defaultProtect = getProtection();
	}

	const C getFirstValue(){
		return firstValue;
	}

	void restoreFirstValue(){
		readLink() = firstValue;
	}

	unsigned int getProtection(){
		DWORD protection;
		VirtualProtect((void*)addr(), sizeof(C), PAGE_EXECUTE_READWRITE, &protection);
		VirtualProtect((void*)addr(), sizeof(C), protection, nullptr);
		return protection;
	}

	void setProtection(unsigned int protect){
		VirtualProtect((void*)addr(), sizeof(C), protect, nullptr);
	}

	void unsetAllProtection(){
		VirtualProtect((void*)addr(), sizeof(C), PAGE_EXECUTE_READWRITE, nullptr);
	}

	void restoreProtection(){
		VirtualProtect((void*)addr(), sizeof(C), defaultProtect, nullptr);
	}

	C& operator()() {
		if (!hasFirstValueSaved){
			firstValue = read();
			hasFirstValueSaved = true;
		}
		return readLink();
	}

protected:
	C& readLink()
	{
		return static_cast<C&>(*reinterpret_cast<C*>(addr()));
	}
	const C read()
	{
		return *reinterpret_cast<C*>(addr());
	}
	unsigned int addr()
	{
		return reinterpret_cast<unsigned int>(module) + offset;
	}

private:
	unsigned int offset;
	HMODULE module;
	C firstValue;
	unsigned int defaultProtect;
	bool hasFirstValueSaved;
};

#endif // MVAR_H
